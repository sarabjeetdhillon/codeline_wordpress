<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'codeline_wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'f lppb#6c.@AfSI*KwM)tN$nmFF @Mmc`Q]6w)GGuL!`~T?YUn-$]+uhyS)!A&{C');
define('SECURE_AUTH_KEY',  '8 VcM!ghpza[Of/0L9R2MX:R:47GYa; 4&F2{ !%z K/+JMY_~WTg2* Y PnzF!j');
define('LOGGED_IN_KEY',    'vFx($;0}BY$}h);xIEcQsk!NlQ3{h2^GZ(D!)*v,GUiQZ</uI[.zlR1xy[Y/yLF[');
define('NONCE_KEY',        'vI1b!hPg^Ie;Wi<e ^|=HQNM;+bPP.xfzw<4np+l&V=+1OO#dk$O*pKRj:^(C2J/');
define('AUTH_SALT',        'YF=wPg}|`gf,)$<:j5.!z]>V%H:q-EzkBx=2JO<_3]DjWFi(ds:5aCHTJ]MZ}L-^');
define('SECURE_AUTH_SALT', ' Jb(=15vb,t}Td)+8x9iBBpsqv ^Ikkihf/VO?,,eid64SwCt>#R8H1v@<hrsDO6');
define('LOGGED_IN_SALT',   'EFUXo3SzgE*#HuW8JYbOY qT8<4zJIh6C!<mCuXR4V&aF0l4bl1n ~wJM3LF`rY[');
define('NONCE_SALT',       'Bxyag8+n0}0xL :Nr.f TV A}nq ?.UkU4>,u+/eQG::3BS+dz2@kvC.*+}_)ZYU');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
