<?php

function create_posttype() {
 
    register_post_type( 'films',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Films' ),
                'singular_name' => __( 'Film' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'films'),
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );

function themes_taxonomy() {  
    register_taxonomy(  
        'genre',  
        'films',  
        array(  
            'hierarchical' => true,  
            'label' => 'Genre',
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'films', 
                'with_front' => false 
			),
		
        )  
    );  
    register_taxonomy(  
        'country',  
        'films',  
        array(  
           
            'label' => 'Country',
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'films', 
                
			),
		
        )  
    );  
    register_taxonomy(  
        'year',  
        'films',  
        array(  
            
            'label' => 'Year',
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'films', 
               
			),
		
        )  
    );  
    register_taxonomy(  
        'actors',  
        'films',  
        array(  
            
            'label' => 'Actors',
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'films', 
               
			),
		
        )  
    );  
}  
add_action( 'init', 'themes_taxonomy');

function wpdocs_custom_taxonomies_terms_links() {
    // Get post by post ID.
    if ( ! $post = get_post() ) {
        return '';
    }
 
    // Get post type by post.
    $post_type = $post->post_type;
 
    // Get post type taxonomies.
    $taxonomies = get_object_taxonomies( $post_type, 'objects' );
 
    $out = array();
 
    foreach ( $taxonomies as $taxonomy_slug => $taxonomy ){
 
        // Get the terms related to post.
        $terms = get_the_terms( $post->ID, $taxonomy_slug );
 
        if ( ! empty( $terms ) ) {
            $out[] = "<h2>" . $taxonomy->label . "</h2>\n<ul>";
            foreach ( $terms as $term ) {
                $out[] = sprintf( '<li><a href="%1$s">%2$s</a></li>',
                    esc_url( get_term_link( $term->slug, $taxonomy_slug ) ),
                    esc_html( $term->name )
                );
            }
            $out[] = "\n</ul>\n";
        }
    }
    return implode( '', $out );
}

function get_custom_fields_hook( $output ) {

	$genre = get_the_terms( $post->ID, 'genre' );
	$genre_terms_string = join(', ', wp_list_pluck($genre, 'name'));

	$country = get_the_terms( $post->ID, 'country' );
	$country_terms_string = join(', ', wp_list_pluck($country, 'name'));

	$ticket_price = get_field('ticket_price');

	$release_date = get_field('release_date');
	

	$output .='<ul>
		<li><b>Genre:</b> '.$genre_terms_string.'</li>
		
		<li><b>Country:</b> '.$country_terms_string.'</li>					
		<li><b>Ticket Price:</b> '.$ticket_price.'</li>	
		<li><b>Release Date:</b> '.date('d-M-Y', strtotime($release_date)).'</li>	
	</ul>
	';
	
	return $output;
  }
  add_filter( 'the_content', 'get_custom_fields_hook' );


  add_shortcode( 'films', 'films_shortcode' );

  function films_shortcode($atts)
  {
	
	$query = new WP_Query( array(
        'post_type' => 'films',
		'order' => 'ASC',
		'posts_per_page'=>$atts['count']
	) );
	$html = '<ul>';
	while ( $query->have_posts() ) : $query->the_post();
	$html .= '<li><a href="'.get_the_permalink().'">'.
		get_the_title()		
	.'</a></li>';
	endwhile;
	$html .='</ul>';
	wp_reset_query();  

	return $html;
  }